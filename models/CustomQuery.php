<?php

namespace app\models;

use yii\db\ActiveQuery;

/**
 * Extended ActiveQuery.
 *
 * @author Anatoly Milkov <anatoly.milko@gmail.com>
 */
class CustomQuery extends ActiveQuery
{
    /**
     * Add condition: STATUS_ACTIVE.
     * @return \app\models\CustomQuery
     */
    public function active()
    {
        return $this->andWhere(['status' => Token::STATUS_ACTIVE]);
    }

    /**
     * Add condition: token's timeExpire less then now().
     * @return \app\models\CustomQuery
     */
    public function actual()
    {
        return $this->andWhere(['>', 'timeExpire', date(\DateTime::W3C)]);
    }

}
