<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\base\UserException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Author model.
 *
 * @property int $authorId
 * @property string $name
 *
 * @property Quote[] $quotes
 *
 * @author Anatoly Milkov <anatoly.milko@gmail.com>
 */
class Author extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{Author}}';
    }

    /**
     * @inheritdoc
     * @return CustomQuery the active query used by this AR class
     */
    public static function find()
    {
        return new CustomQuery(get_called_class());
    }

    /**
     * Relation with Token table .
     * @return CustomQuery
     */
    public function getQuotes()
    {
        return $this->hasMany(Quote::class, ['authorId' => 'authorId'])
                ->inverseOf('author');
    }

}
