<?php

namespace app\models;

use yii\base\Model;

/**
 * LoginForm.
 *
 * @property \app\models\User $user Related User model
 *
 * @author Anatoly Milkov <anatoly.milko@gmail.com>
 */
class LoginForm extends Model
{
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $password;
    /**
     * @var \app\modules\user\models\User
     */
    private $user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // email and password are both required
            [['email', 'password'], 'required'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Wrong email or password.');
            }
        }
    }

    /**
     * Finds user by email.
     * @return \app\models\User|null
     */
    public function getUser()
    {
        if (!$this->user) {
            $this->user = User::findByEmail($this->email);
        }
        return $this->user;
    }

}
