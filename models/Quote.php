<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Quote model.
 *
 * @property int $quoteId
 * @property int $authorId
 * @property string $quote
 *
 * @property Author $author
 *
 * @author Anatoly Milkov <anatoly.milko@gmail.com>
 */
class Quote extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{Quote}}';
    }

    /**
     * @inheritdoc
     * @return \app\models\CustomQuery the active query used by this AR class
     */
    public static function find()
    {
        return new CustomQuery(get_called_class());
    }

    /**
     * Relation with Author table .
     * @return \app\models\CustomQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Author::class, ['authorId' => 'authorId'])
                ->inverseOf('quotes');
    }

}
