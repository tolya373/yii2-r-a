<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Token model.
 *
 * @property int $tokenId
 * @property int $userId
 * @property string $token
 * @property string $status
 * @property string $timeCreated
 * @property string $timeExpire
 *
 * @property User $user
 *
 * @author Anatoly Milkov <anatoly.milko@gmail.com>
 */
class Token extends ActiveRecord
{
    const STATUS_ACTIVE  = 'active';
    const STATUS_DELETED = 'deleted';
    /**
     * Period of token validity time (in hours).
     */
    const EXPIRE_PERIOD  = 48;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{Token}}';
    }

    /**
     * @inheritdoc
     * @return \app\models\CustomQuery the active query used by this AR class
     */
    public static function find()
    {
        return new CustomQuery(get_called_class());
    }

    /**
     * Relation with User table .
     * @return \app\models\CustomQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['userId' => 'userId'])
                ->inverseOf('tokens');
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            $this->status     = static::STATUS_ACTIVE;
            $this->token      = static::generateTokenString();
            $this->timeExpire = (new \DateTime())
                ->add(new \DateInterval(sprintf('PT%sH', static::EXPIRE_PERIOD)))
                ->format(\DateTime::W3C);
        }
        return true;
    }

    /**
     * Generates unique secure string for the new Token.
     * @return string
     */
    public static function generateTokenString()
    {
        while (true) {
            $string = Yii::$app->security->generateRandomString(64);
            if (static::find()->where(['token' => $string])->exists()) {
                continue;
            }
            return $string;
        }
    }

    /**
     * @param string $token
     * @return Token|null
     */
    public static function getActiveByToken($token)
    {
        return static::find()
                ->where(['token' => $token])
                ->active()
                ->actual()
                ->one();
    }

    /**
     * @return true
     */
    public function markDeleted()
    {
        $this->status = static::STATUS_DELETED;
        return $this->save(false);
    }

}
