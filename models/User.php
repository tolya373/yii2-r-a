<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\base\UserException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model.
 *
 * @property int $userId
 * @property string $email
 * @property string $passwordHash
 * @property string $info
 * @property string $status
 * @property string $timeCreated
 *
 * @property Token[] $tokens
 *
 * @author Anatoly Milkov <anatoly.milko@gmail.com>
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_ACTIVE  = 'active';
    const STATUS_DELETED = 'deleted';

    /** @var string */
    private $_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{User}}';
    }

    /**
     * @inheritdoc
     * @return CustomQuery the active query used by this AR class
     */
    public static function find()
    {
        return new CustomQuery(get_called_class());
    }

    /**
     * Relation with Token table .
     * @return CustomQuery
     */
    public function getTokens()
    {
        return $this->hasMany(Token::class, ['userId' => 'userId'])
                ->inverseOf('user');
    }

    public function scenarios()
    {
        return [
            'create' => ['email', 'password', 'info'],
        ];
    }

    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            [['email', 'password', 'info'], 'trim'],
            [['email'], 'email'],
            [['email'], 'unique'],
            [['info'], 'string', 'max' => 1024],
            [['password'], 'string', 'max' => 64],
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if ($this->isNewRecord) {
            if ($this->scenario == 'create') {
                $this->status = static::STATUS_ACTIVE;
            }
        }
        return true;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->_password;
    }

    /**
     * Generates password hash from password and sets it to the model.
     * @param string $password
     */
    public function setPassword($password)
    {
        if ($password && !YII_ENV_TEST) {
            $this->_password    = $password;
            $this->passwordHash = Yii::$app->security->generatePasswordHash($password);
        }
        // We don't wont to generate secure hash while testing. It takes too long.
        elseif (YII_ENV_TEST) {
            $this->_password    = $password;
            $this->passwordHash = $password;
        }
    }

    /**
     * Finds user by email.
     * @param  string $email
     * @return static|null \app\models\User
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Validates password.
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        if (YII_ENV_TEST) {
            return $password === $this->passwordHash;
        } else {
            return Yii::$app->security->validatePassword($password, $this->passwordHash);
        }
    }

    /**
     * @return Token
     */
    public function getActiveToken()
    {
        $token = $this->getTokens()->active()->actual()->orderBy('timeExpire')->one();
        if (!$token) {
            $token         = new Token();
            $token->userId = $this->userId;
            if (!$token->save()) {
                throw new UserException('Cannot save new Token');
            }
        }
        return $token;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        throw new NotSupportedException(__CLASS__ . '::' . __METHOD__);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $tokenModel = Token::getActiveByToken($token);
        if (!$tokenModel) {
            return null;
        }
        return $tokenModel->user;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->userId;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        throw new NotSupportedException(__CLASS__ . '::' . __METHOD__);
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        throw new NotSupportedException(__CLASS__ . '::' . __METHOD__);
    }

}
