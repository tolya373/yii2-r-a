<?php

use app\tests\fixtures\AuthorFixture;
use app\tests\fixtures\QuoteFixture;
use app\tests\fixtures\TokenFixture;
use app\tests\fixtures\UserFixture;
use Codeception\Util\HttpCode;

class ApiCest
{
    private $registerData = [
        'email'    => 'alexey@plumflowerinternational.com',
        'password' => 'lkJlkn8hj',
        'info'     => 'I’m experienced <b>web</b> developer!',
    ];

    public function _before(ApiTester $I)
    {
        return;
    }

    public function _after(ApiTester $I)
    {
        return;
    }

    public function getWrongRouteTest(ApiTester $I)
    {
        $I->sendGET('api/some/wrong/path');
        $I->seeResponseCodeIs(HttpCode::NOT_FOUND);
    }

    public function getInfoTest(ApiTester $I)
    {
        $I->sendGET('api/info');
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => true]);
    }

    public function postRegisterTest(ApiTester $I)
    {
        $I->wantTo('create a user');
        $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
        $I->sendPOST('api/register', $this->registerData);
        $I->seeResponseCodeIs(HttpCode::CREATED);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => true]);
    }

    public function postLoginTest(ApiTester $I)
    {
        $I->haveFixtures([
            'users'  => UserFixture::class,
            'tokens' => TokenFixture::class,
        ]);

        $user  = $I->grabFixture('users', 'user1');
        $token = $I->grabFixture('tokens', 'token1');

        $I->wantTo('log in a user');

        $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
        $I->sendPOST('api/login', [
            'email'    => $user->email,
            'password' => $user->passwordHash,
        ]);
        $I->seeResponseCodeIs(HttpCode::CREATED);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'success' => true,
            'data'    => [
                'token' => $token->token
            ],
        ]);

        /**
         * Rolling back fixtures coz Yii2+Codeception can't do it.
         * https://github.com/yiisoft/yii2-codeception/issues/7
         */
        (new TokenFixture())->after();
        (new UserFixture())->after();
    }

    public function deleteLogoutTest(ApiTester $I)
    {
        $I->haveFixtures([
            'users'  => UserFixture::class,
            'tokens' => TokenFixture::class,
        ]);

        $user  = $I->grabFixture('users', 'user1');
        $token = $I->grabFixture('tokens', 'token1');

        $I->wantTo('log out a user');

        $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
        $url = sprintf('api/logout&token=%s', $token->token);
        $I->sendDELETE($url);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => true]);

        /**
         * Rolling back fixtures coz Yii2+Codeception can't do it.
         * https://github.com/yiisoft/yii2-codeception/issues/7
         */
        (new TokenFixture())->after();
        (new UserFixture())->after();
    }

    public function getProfileTest(ApiTester $I)
    {
        $I->haveFixtures([
            'users'  => UserFixture::class,
            'tokens' => TokenFixture::class,
        ]);

        $user  = $I->grabFixture('users', 'user1');
        $token = $I->grabFixture('tokens', 'token1');

        $I->wantTo('get profile of a user');

        $url = sprintf('api/profile&token=%s', $token->token);
        $I->sendGET($url);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'success' => true,
            'data'    => [
                'email' => $user->email,
                'info'  => $user->info,
            ],
        ]);

        /**
         * Rolling back fixtures coz Yii2+Codeception can't do it.
         * https://github.com/yiisoft/yii2-codeception/issues/7
         */
        (new TokenFixture())->after();
        (new UserFixture())->after();
    }

    public function getAuthorTest(ApiTester $I)
    {
        $I->haveFixtures([
            'users'   => UserFixture::class,
            'tokens'  => TokenFixture::class,
            'authors' => AuthorFixture::class,
        ]);

        $user  = $I->grabFixture('users', 'user1');
        $token = $I->grabFixture('tokens', 'token1');

        $I->wantTo('get random author');

        $url = sprintf('api/author&token=%s', $token->token);
        $I->sendGET($url);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => true]);

        /**
         * Rolling back fixtures coz Yii2+Codeception can't do it.
         * https://github.com/yiisoft/yii2-codeception/issues/7
         */
        (new AuthorFixture())->after();
        (new TokenFixture())->after();
        (new UserFixture())->after();
    }

    public function getQuoteTest(ApiTester $I)
    {
        $I->haveFixtures([
            'users'   => UserFixture::class,
            'tokens'  => TokenFixture::class,
            'authors' => AuthorFixture::class,
            'quotes'  => QuoteFixture::class,
        ]);

        $user   = $I->grabFixture('users', 'user1');
        $token  = $I->grabFixture('tokens', 'token1');
        $author = $I->grabFixture('authors', 'author1');

        $I->wantTo('get random quote of author authorId');

        $url = sprintf('api/quote&authorId=%s&token=%s', $author->authorId, $token->token);
        $I->sendGET($url);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => true]);

        /**
         * Rolling back fixtures coz Yii2+Codeception can't do it.
         * https://github.com/yiisoft/yii2-codeception/issues/7
         */
        (new QuoteFixture())->after();
        (new AuthorFixture())->after();
        (new TokenFixture())->after();
        (new UserFixture())->after();
    }

}
