<?php

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

/**
 * AbstractActiveFixture
 *
 * @author Anatoly Milkov <anatoly.milko@gmail.com>
 */
abstract class AbstractActiveFixture extends ActiveFixture
{
    public function after()
    {
        $this->resetTable();
    }
}
