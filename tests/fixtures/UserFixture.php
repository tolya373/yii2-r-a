<?php

namespace app\tests\fixtures;

use app\models\User;

/**
 * UserFixture
 *
 * @author Anatoly Milkov <anatoly.milko@gmail.com>
 */
class UserFixture extends AbstractActiveFixture
{
    public $modelClass = User::class;

}
