<?php
return [
    'token1' => [
        'tokenId'     => 1,
        'userId'      => 1,
        'token'       => 'veryrandomlonglongstring',
        'status'      => 'active',
        'timeCreated' => date(\DateTime::W3C),
        'timeExpire'  => (new \DateTime())
            ->add(new \DateInterval(sprintf('PT%sH', 48)))
            ->format(\DateTime::W3C),
    ],
];
