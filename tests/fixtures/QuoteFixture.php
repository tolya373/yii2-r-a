<?php

namespace app\tests\fixtures;

use app\models\Quote;

/**
 * QuoteFixture
 *
 * @author Anatoly Milkov <anatoly.milko@gmail.com>
 */
class QuoteFixture extends AbstractActiveFixture
{
    public $modelClass = Quote::class;
    public $depends    = [
        AuthorFixture::class,
    ];

}
