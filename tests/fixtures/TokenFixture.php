<?php

namespace app\tests\fixtures;

use app\models\Token;

/**
 * TokenFixture
 *
 * @author Anatoly Milkov <anatoly.milko@gmail.com>
 */
class TokenFixture extends AbstractActiveFixture
{
    public $modelClass = Token::class;
    public $depends    = [
        UserFixture::class,
    ];

}
