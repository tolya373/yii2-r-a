<?php

namespace app\tests\fixtures;

use app\models\Author;

/**
 * AuthorFixture
 *
 * @author Anatoly Milkov <anatoly.milko@gmail.com>
 */
class AuthorFixture extends AbstractActiveFixture
{
    public $modelClass = Author::class;

}
