<?php

namespace app\controllers;

use app\models\Author;
use app\models\LoginForm;
use app\models\Token;
use app\models\User;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ApiController
 *
 * @author Anatoly Milkov <anatoly.milko@gmail.com>
 */
class ApiController extends AbstractAuthController
{
    /**
     * @inheritdoc
     */
    public function getOpenActions()
    {
        return ['info', 'register', 'login'];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors                      = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class'   => ContentNegotiator::class,
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['verbFilter']        = [
            'class'   => VerbFilter::class,
            'actions' => [
                'info'     => ['get'],
                'register' => ['post'],
                'login'    => ['post'],
                'logout'   => ['delete'],
                'profile'  => ['get'],
                'author'   => ['get'],
                'quote'    => ['get'],
            ],
        ];

        if ($authenticator = $this->getAuthenticatorBehavior()) {
            $behaviors['authenticator'] = $authenticator;
        }

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'rules' => [
                [
                    'allow'   => true,
                    'actions' => ['info'],
                    'roles'   => ['?', '@'],
                ],
                [
                    'allow'   => true,
                    'actions' => ['register', 'login'],
                    'roles'   => ['?'],
                ],
                [
                    'allow'   => true,
                    'actions' => ['logout', 'profile', 'author', 'quote'],
                    'roles'   => ['@'],
                ],
                [
                    'allow' => false,
                ],
            ],
        ];
        return $behaviors;
    }

    /**
     * @param bool $success
     * @param array $data
     * @return array
     */
    protected static function dataWrapper($success, $data)
    {
        return [
            'success' => $success,
            'data'    => $data,
        ];
    }

    public function actionInfo()
    {
        if (!isset(Yii::$app->params['companyInfo'])) {
            throw new InvalidConfigException('params.companyInfo');
        }
        return static::dataWrapper(
                true, ['info' => Yii::$app->params['companyInfo']]
        );
    }

    public function actionRegister()
    {
        $user             = new User(['scenario' => 'create']);
        $user->attributes = Yii::$app->request->getBodyParams();
        if (!$user->save()) {
            Yii::$app->getResponse()->setStatusCode(400);
            return static::dataWrapper(
                    false, ['errors' => $user->getErrors()]
            );
        }
        Yii::$app->getResponse()->setStatusCode(201);
        return static::dataWrapper(true, []);
    }

    public function actionLogin()
    {
        $loginForm             = new LoginForm();
        $loginForm->attributes = Yii::$app->request->getBodyParams();
        if (!$loginForm->validate()) {
            Yii::$app->getResponse()->setStatusCode(400);
            return static::dataWrapper(
                    false, ['errors' => $loginForm->getErrors()]
            );
        }

        $token = $loginForm->user->getActiveToken();

        Yii::$app->getResponse()->setStatusCode(201);
        return static::dataWrapper(true, ['token' => $token->token]);
    }

    public function actionLogout()
    {
        $token = Token::findOne(['token' => Yii::$app->request->get('token')]);
        if (!$token) {
            Yii::$app->getResponse()->setStatusCode(400);
            return static::dataWrapper(
                    false, ['errors' => 'Can not find session Token']
            );
        }

        if (!$token->markDeleted()) {
            Yii::$app->getResponse()->setStatusCode(400);
            return static::dataWrapper(
                    false, ['errors' => 'Error deleting Token']
            );
        }

        Yii::$app->getResponse()->setStatusCode(200);
        return static::dataWrapper(true, []);
    }

    public function actionProfile()
    {
        return static::dataWrapper(
                true, [
                'email' => Yii::$app->user->identity->email,
                'info'  => Yii::$app->user->identity->info,
                ]
        );
    }

    public function actionAuthor()
    {
        $author = Author::find()->orderBy('RAND()')->one();
        if (!$author) {
            throw new NotFoundHttpException('Author');
        }
        if (!YII_ENV_TEST) {
            sleep(5);
        }
        return static::dataWrapper(true, $author->toArray());
    }

    public function actionQuote()
    {
        $authorId = (int) Yii::$app->request->get('authorId');
        if (!$authorId) {
            throw new \yii\web\BadRequestHttpException('authorId');
        }
        $author = Author::findOne(['authorId' => $authorId]);
        if (!$author) {
            throw new \yii\web\BadRequestHttpException('author');
        }
        $quote = $author->getQuotes()->orderBy('RAND()')->one();
        if (!$quote) {
            throw new NotFoundHttpException('Quote');
        }
        if (!YII_ENV_TEST) {
            sleep(5);
        }
        return static::dataWrapper(true, $quote->toArray());
    }

}
