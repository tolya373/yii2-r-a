<?php

namespace app\controllers;

use yii\filters\auth\QueryParamAuth;
use yii\rest\Controller;

/**
 * AbstractAuthController
 *
 * @author Anatoly Milkov <anatoly.milko@gmail.com>
 */
abstract class AbstractAuthController extends Controller
{
    /**
     * @return array List of action ids with no authentication needed.
     */
    abstract public function getOpenActions();
    /**
     * Returns config of 'authenticator' behavior.
     * @return array
     */
    protected function getAuthenticatorBehavior()
    {
        if (!in_array($this->action->id, $this->getOpenActions())) {
            return [
                'class'      => QueryParamAuth::class,
                'tokenParam' => 'token',
            ];
        }
        return [];
    }

}
