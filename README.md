# Yii2 app with RESTful API #

## Configuring ##

* Create MySQL databases (main and test).
* Load vendors via composer.
* Initialize codeception.

    `$ php vendor/bin/codecept buil`

## Preparing ##

Apply migrations to test DB:

`$ ./tests/bin/yii migrate`

## Run tests ##

`$ php vendor/bin/codecept run api`