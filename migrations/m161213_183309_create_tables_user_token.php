<?php

use yii\db\Migration;

class m161213_183309_create_tables_user_token extends Migration
{
    public function up()
    {
        $this->createTable('User', [
            'userId'       => $this->primaryKey(),
            'email'        => $this->string(128)->notNull(),
            'passwordHash' => $this->string(128)->notNull(),
            'info'         => $this->text(),
            'status'       => $this->string(),
            'timeCreated'  => $this->timestamp()->notNull()->defaultExpression('now()'),
        ]);
        $this->createTable('Token', [
            'tokenId'     => $this->primaryKey(),
            'userId'      => $this->integer()->notNull(),
            'token'       => $this->string(128)->notNull()->unique(),
            'status'      => $this->string()->notNull(),
            'timeCreated' => $this->timestamp()->notNull()->defaultExpression('now()'),
            'timeExpire'  => $this->dateTime()->notNull(),
        ]);
        // add foreign key for table `User`
        $this->addForeignKey(
            'TokenUserIdUserFK', 'Token', 'userId', 'User', 'userId', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('TokenUserIdUserFK', 'Token');
        $this->dropTable('Token');
        $this->dropTable('User');
    }

}
