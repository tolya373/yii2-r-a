<?php

use yii\db\Migration;

class m161215_195340_create_table_author extends Migration
{
    public function safeUp()
    {
        $this->createTable('Author', [
            'authorId' => $this->primaryKey(),
            'name'     => $this->string(128)->notNull(),
        ]);
        foreach ($this->getData() as $columns) {
            $this->insert('Author', $columns);
        }
    }

    public function safeDown()
    {
        $this->dropTable('Author');
    }

    private function getData()
    {
        return [
            [
                'authorId' => 1,
                'name'     => 'Walt Disney',
            ], [
                'authorId' => 2,
                'name'     => 'Mark Twain',
            ], [
                'authorId' => 3,
                'name'     => 'Albert Einstein',
            ]
        ];
    }

}
