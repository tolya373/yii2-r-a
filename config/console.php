<?php
$params = require(__DIR__ . '/params.php');
$db     = require(__DIR__ . '/db.php');

$config = [
    'id'                  => 'yii2-r-a-console',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'app\commands',
    'components'          => [
        'cache' => [
            'class' => \yii\caching\FileCache::class,
        ],
        'log'   => [
            'targets' => [
                [
                    'class'  => \yii\log\FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db'    => $db,
    ],
    'params'              => $params,
    /*
      'controllerMap' => [
      'fixture' => [ // Fixture generation command line.
      'class' => 'yii\faker\FixtureController',
      ],
      ],
     */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
}

return $config;
